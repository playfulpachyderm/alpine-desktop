alpine download url: https://dl-cdn.alpinelinux.org/alpine/v3.15/releases/x86_64/alpine-standard-3.15.3-x86_64.iso

In Virtualbox
-------------

New
- Name: Alpine
- Type: Linux
- Version: Other Linux (64-bit)

Memory: 2048 MB

Disk: VDI, Dynamically allcoated, 10 GB

Go to Settings on new machine.  
System > Motherboard > Boot Order: ensrue "optical" is top, then hard disk
System > Processor > Processor(s): set to 2
Storage > Controller: IDE > click on the optical disk, use the dropdown (the CD icon) on the right to put the ISO on it (press "Create New" if necessary)

Start the VM

Login: "root" (no password is needed)

setup-alpine

Keyboard layout: us us
Hostname: alpine
Interface: eth0
IP address: dhcp
Manual network config: no
Set root password: root
Timezone: US/Pacific
Proxy URL: none
NTP: busybox
Mirror number: r
SSH server: openssh
Disk to use: sda
How to use the disk: sys
Erase the disk and continue?: y


poweroff

Remove the optical disk (settings: Storage), then start the machine again

Edit the /etc/apk/repositories file to uncomment the "Community" one
run `apk update`


Install KDE:
run `setup-xorg-base`
apk add plasma elogind polkit-elogind dbus  # This is a large install with extras; for minimal install see "Lightweight plasma installation": https://wiki.alpinelinux.org/wiki/KDE
rc-update add dbus
rc-update add elogind
rc-update add polkit
rc-update add udev

Start the display manager, which will begin a desktop session:
rc-update add sddm
rc-service sddm start

Use "Plasma (X11)" since Wayland doesn't seem to be enabled by default  (TODO)

Resize screen: https://selivan.github.io/2019/01/31/virtualbox-linux-guest-autoresize-screen.html

Add some packages
apk add git go firefox sqlite build-base

apk add qt5-qtbase-dev qt5-qtmultimedia-dev
